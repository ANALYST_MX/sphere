#include "exception/Exception.hxx"

#include <cstdarg>
#include <cstring>
#include <iostream>

namespace sphere
{
  namespace exception
  {

    Exception::Exception(const char *fmt, ...) throw()
    {
      va_list args;
      va_start(args, fmt);
      char *ret = nullptr;
      if (vasprintf(&ret, fmt, args) < 0)
	{
	  std::cerr << strerror(errno) << std::endl;
	
	  exit(EXIT_FAILURE);
	}
      m_message = ret;
      free(ret);
      va_end(args);
    }
    
    Exception::Exception(const Exception& other) throw()
      : std::exception(other)
    {
      m_message = other.m_message;
    }
    
    Exception::~Exception() throw()
    {
    }

    const char *Exception::what() const throw()
    {
      return m_message.c_str();
    }

    std::string Exception::toString() const
    {
      return m_message;
    }

    char const *Exception::type() const
    {
      return "Exception";
    }

  }
}
