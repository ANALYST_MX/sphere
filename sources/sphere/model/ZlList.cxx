#include "model/ZlList.hxx"

#include <algorithm>

namespace sphere
{
  namespace model
  {

    ZlList::~ZlList()
    {
    }
    
    std::shared_ptr<Zglv> ZlList::getZglv() const
    {
      return m_zglv;
    }
    
    std::shared_ptr<Schet> ZlList::getSchet() const
    {
      return m_schet;
    }
    
    std::vector<std::shared_ptr<Zap> > &ZlList::getZap()
    {
      return m_zap;
    }
    
    const std::vector<std::shared_ptr<Zap> > &ZlList::getZap() const
    {
      return m_zap;
    }
    
    void ZlList::setZglv(std::shared_ptr<Zglv> zglv)
    {
      m_zglv = zglv;
    }
    
    void ZlList::setSchet(std::shared_ptr<Schet> schet)
    {
      m_schet = schet;
    }
    
    bool ZlList::addZap(std::shared_ptr<Zap> zap)
    {
      auto result{ false };
      m_zap.emplace_back(zap);
      result = true;
      
      return result;
    }
    
    bool ZlList::removeZap(std::shared_ptr<Zap> zap)
    {
      auto result{ false };
      m_zap.erase(std::remove_if(m_zap.begin(), m_zap.end(), [&](std::shared_ptr<Zap> found)
				  {
				    result = found == zap;
				    return result;
				  }
				  ), m_zap.end());

      return result;
    }

  }
}
