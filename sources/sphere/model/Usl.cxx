#include "model/Usl.hxx"

namespace sphere
{
  namespace model
  {

    Usl::~Usl()
    {
    }

    inline std::string Usl::getIdserv() const
    {
      return m_idserv;
    }
    
    inline std::string Usl::getLpu() const
    {
      return m_lpu;
    }
    
    inline int Usl::getLpu1() const
    {
      return m_lpu1;
    }
    
    inline int Usl::getPodr() const
    {
      return m_podr;
    }
    
    inline int Usl::getProfil() const
    {
      return m_profil;
    }
    
    inline std::string Usl::getVidVme() const
    {
      return m_vidVme;
    }
    
    inline int Usl::getDet() const
    {
      return m_det;
    }
    
    sphere::type::Date Usl::getDateIn() const
    {
      return m_dateIn;
    }
    
    sphere::type::Date Usl::getDateOut() const
    {
      return m_dateOut;
    }
    
    inline std::string Usl::getDs() const
    {
      return m_ds;
    }
    
    inline std::string Usl::getCodeUsl() const
    {
      return m_codeUsl;
    }
    
    inline double Usl::getKolUsl() const
    {
      return m_kolUsl;
    }
    
    sphere::type::Money Usl::getTarif() const
    {
      return m_tarif;
    }
    
    sphere::type::Money Usl::getSumvUsl() const
    {
      return m_sumvUsl;
    }
    
    inline int Usl::getPrvs() const
    {
      return m_prvs;
    }
    
    inline std::string Usl::getCodeMd() const
    {
      return m_codeMd;
    }
    
    inline std::string Usl::getComentu() const
    {
      return m_comentu;
    }

    inline void Usl::setIdserv(const std::string &idserv)
    {
      m_idserv = idserv;
    }
    
    inline void Usl::setLpu(const std::string &lpu)
    {
      m_lpu = lpu;
    }
    
    inline void Usl::setLpu1(int lpu1)
    {
      m_lpu1 = lpu1;
    }
    
    inline void Usl::setPodr(int podr)
    {
      m_podr = podr;
    }
    
    inline void Usl::setProfil(int profil)
    {
      m_profil = profil;
    }
    
    inline void Usl::setVidVme(const std::string &vidVme)
    {
      m_vidVme = vidVme;
    }
    
    inline void Usl::setDet(int det)
    {
      m_det = det;
    }
    
    void Usl::setDateIn(const sphere::type::Date &dateIn)
    {
      m_dateIn = dateIn;
    }
    
    void Usl::setDateOut(const sphere::type::Date &dateOut)
    {
      m_dateOut = dateOut;
    }
    
    inline void Usl::setDs(const std::string &ds)
    {
      m_ds = ds;
    }
    
    inline void Usl::setCodeUsl(const std::string &codeUsl)
    {
      m_codeUsl = codeUsl;
    }
    
    inline void Usl::setKolUsl(double kolUsl)
    {
      m_kolUsl = kolUsl;
    }
    
    void Usl::setTarif(const sphere::type::Money &tarif)
    {
      m_tarif = tarif;
    }
    
    void Usl::setSumvUsl(const sphere::type::Money &sumvUsl)
    {
      m_sumvUsl = sumvUsl;
    }
    
    inline void Usl::setPrvs(int prvs)
    {
      m_prvs = prvs;
    }
    
    inline void Usl::setCodeMd(const std::string &codeMd)
    {
      m_codeMd = codeMd;
    }
    
    inline void Usl::setComentu(const std::string &comentu)
    {
      m_comentu = comentu;
    }
    
  }
}
