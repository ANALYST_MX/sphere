#include "model/Sank.hxx"

namespace sphere
{
  namespace model
  {

    Sank::~Sank()
    {
    }

    inline std::string Sank::getSCode() const
    {
      return m_sCode;
    }
    
    sphere::type::Money Sank::getSSum() const
    {
      return m_sSum;
    }
    
    inline int Sank::getSTip() const
    {
      return m_sTip;
    }
    
    inline int Sank::getSOsn() const
    {
      return m_sOsn;
    }

    inline std::string Sank::getSCom() const
    {
      return m_sCom;
    }
    
    inline int Sank::getSIst() const
    {
      return m_sIst;
    }
    
    inline void Sank::setSCode(const std::string &sCode)
    {
      m_sCode = sCode;
    }
    
    void Sank::setSSum(const sphere::type::Money &sSum)
    {
      m_sSum = sSum;
    }
    
    inline void Sank::setSTip(int sTip)
    {
      m_sTip = sTip;
    }
    
    inline void Sank::setSOsn(int sOsn)
    {
      m_sOsn = sOsn;
    }
    
    inline void Sank::setSCom(const std::string &sCom)
    {
      m_sCom = sCom;
    }
    
    inline void Sank::setSIst(int sIst)
    {
      m_sIst = sIst;
    }

  }
}
      
