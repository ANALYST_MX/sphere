#include "model/Schet.hxx"

namespace sphere
{
  namespace model
  {
    
    Schet::~Schet()
    {
    }
    
    inline int Schet::getCode() const
    {
      return m_code;
    }
    
    inline std::string Schet::getCodeMo() const
    {
      return m_codeMo;
    }
    
    inline int Schet::getPodr() const
    {
      return m_podr;
    }
    
    inline int Schet::getYear() const
    {
      return m_year;
    }
    
    inline int Schet::getMonth() const
    {
      return m_month;
    }
    
    inline std::string Schet::getNschet() const
    {
      return m_nschet;
    }
    
    sphere::type::Date Schet::getDschet() const
    {
      return m_dschet;
    }
    
    inline std::string Schet::getPlat() const
    {
      return m_plat;
    }
    
    sphere::type::Money Schet::getSummav() const
    {
      return m_summav;
    }
    
    sphere::type::Money Schet::getSummavR() const
    {
      return m_summavR;
    }
    
    sphere::type::Money Schet::getSummavF() const
    {
      return m_summavF;
    }
    
    inline std::string Schet::getComents() const
    {
      return m_coments;
    }
    
    sphere::type::Money Schet::getSummap() const
    {
      return m_summap;
    }
    
    sphere::type::Money Schet::getSummapR() const
    {
      return m_summapR;
    }
    
    sphere::type::Money Schet::getSummapF() const
    {
      return m_summapF;
    }
    
    sphere::type::Money Schet::getSankMek() const
    {
      return m_sankMek;
    }
    
    sphere::type::Money Schet::getSankMee() const
    {
      return m_sankMee;
    }
    
    sphere::type::Money Schet::getSankEkmp() const
    {
      return m_sankEkmp;
    }
    
    inline std::string Schet::getDisp() const
    {
      return m_disp;
    }
    
    inline void Schet::setCode(int code)
    {
      m_code = code;
    }
    
    inline void Schet::setCodeMo(const std::string &codeMo)
    {
      m_codeMo = codeMo;
    }
    
    inline void Schet::setPodr(int podr)
    {
      m_podr = podr;
    }
    
    inline void Schet::setYear(int year)
    {
      m_year = year;
    }
    
    inline void Schet::setMonth(int month)
    {
      m_month = month;
    }
    
    inline void Schet::setNschet(const std::string &nschet)
    {
      m_nschet = nschet;
    }
    
    void Schet::setDschet(const sphere::type::Date &dschet)
    {
      m_dschet = dschet;
    }
    
    inline void Schet::setPlat(const std::string &plat)
    {
      m_plat = plat;
    }
    
    void Schet::setSummav(const sphere::type::Money &summav)
    {
      m_summav = summav;
    }
    
    void Schet::setSummavR(const sphere::type::Money &summavR)
    {
      m_summavR = summavR;
    }
    
    void Schet::setSummavF(const sphere::type::Money &summavF)
    {
      m_summavF = summavF;
    }
    
    inline void Schet::setComents(const std::string &coments)
    {
      m_coments = coments;
    }
    
    void Schet::setSummap(const sphere::type::Money &summap)
    {
      m_summap = summap;
    }
    
    void Schet::setSummapR(const sphere::type::Money &summapR)
    {
      m_summapR = summapR;
    }
    
    void Schet::setSummapF(const sphere::type::Money &summapF)
    {
      m_summapF = summapF;
    }
    
    void Schet::setSankMek(const sphere::type::Money &sankMek)
    {
      m_sankMek = sankMek;
    }
    
    void Schet::setSankMee(const sphere::type::Money &sankMee)
    {
      m_sankMee = sankMee;
    }
    
    void Schet::setSankEkmp(const sphere::type::Money &sankEkmp)
    {
      m_sankEkmp = sankEkmp;
    }
    
    inline void Schet::setDisp(const std::string &disp)
    {
      m_disp = disp;
    }

  }
}
