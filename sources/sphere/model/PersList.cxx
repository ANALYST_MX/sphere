#include "model/PersList.hxx"

#include <algorithm>

namespace sphere
{
  namespace model
  {

    PersList::~PersList()
    {
    }

    std::shared_ptr<Zglv> PersList::getZglv() const
    {
      return m_zglv;
    }
    
    std::vector<std::shared_ptr<Pers> > &PersList::getPers()
    {
      return m_pers;
    }
    
    const std::vector<std::shared_ptr<Pers> > &PersList::getPers() const
    {
      return m_pers;
    }
    
    void PersList::setZglv(std::shared_ptr<Zglv> zglv)
    {
      m_zglv = zglv;
    }
    
    bool PersList::addPers(std::shared_ptr<Pers> pers)
    {
      auto result{ false };
      m_pers.emplace_back(pers);
      result = true;
      
      return result;
    }
    
    bool PersList::removePers(std::shared_ptr<Pers> pers)
    {
      auto result{ false };
      m_pers.erase(std::remove_if(m_pers.begin(), m_pers.end(), [&](std::shared_ptr<Pers> found)
				  {
				    result = found == pers;
				    return result;
				  }
				  ), m_pers.end());

      return result;
    }

  }
}
