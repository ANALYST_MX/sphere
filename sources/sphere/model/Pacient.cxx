#include "model/Pacient.hxx"

namespace sphere
{
  namespace model
  {

    Pacient::~Pacient()
    {
    }
    
    inline std::string Pacient::getIdPac() const
    {
      return m_idPac;
    }
    
    inline int Pacient::getVpolis() const
    {
      return m_vpolis;
    }
    
    inline std::string Pacient::getSpolis() const
    {
      return m_spolis;
    }
    
    inline std::string Pacient::getNpolis() const
    {
      return m_npolis;
    }
    
    inline std::string Pacient::getStOkato() const
    {
      return m_stOkato;
    }
    
    inline std::string Pacient::getSmo() const
    {
      return m_smo;
    }
    
    inline std::string Pacient::getSmoOgrn() const
    {
      return m_smoOgrn;
    }
    
    inline std::string Pacient::getSmoOk() const
    {
      return m_smoOk;
    }
    
    inline std::string Pacient::getSmoNam() const
    {
      return m_smoNam;
    }
    
    inline std::string Pacient::getPrinPol() const
    {
      return m_prinPol;
    }
    
    inline std::string Pacient::getNovor() const
    {
      return m_novor;
    }
    
    inline int Pacient::getVnovD() const
    {
      return m_vnovD;
    }
    
    inline void Pacient::setIdPac(const std::string &idPac)
    {
      m_idPac = idPac;
    }
    
    inline void Pacient::setVpolis(int vpolis)
    {
      m_vpolis = vpolis;
    }
    
    inline void Pacient::setSpolis(const std::string &spolis)
    {
      m_spolis = spolis;
    }
    
    inline void Pacient::setNpolis(const std::string &npolis)
    {
      m_npolis = npolis;
    }
    
    inline void Pacient::setStOkato(const std::string &stOkato)
    {
      m_stOkato = stOkato;
    }
    
    inline void Pacient::setSmo(const std::string &smo)
    {
      m_smo = smo;
    }
    
    inline void Pacient::setSmoOgrn(const std::string &smoOgrn)
    {
      m_smoOgrn = smoOgrn;
    }
    
    inline void Pacient::setSmoOk(const std::string &smoOk)
    {
      m_smoOk = smoOk;
    }
    
    inline void Pacient::setSmoNam(const std::string &smoNam)
    {
      m_smoNam = smoNam;
    }
    
    inline void Pacient::setPrinPol(const std::string &prinPol)
    {
      m_prinPol = prinPol;
    }
    
    inline void Pacient::setNovor(const std::string &novor)
    {
      m_novor = novor;
    }
    
    inline void Pacient::setVnovD(int vnovD)
    {
      m_vnovD = vnovD;
    }

  }
}
      
