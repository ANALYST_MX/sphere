#include "model/Zglv.hxx"

namespace sphere
{
  namespace model
  {

    Zglv::~Zglv()
    {
    }

    inline std::string Zglv::getVersion() const
    {
      return m_version;
    }
    
    sphere::type::Date Zglv::getData() const
    {
      return m_data;
    }
    
    inline std::string Zglv::getFilename() const
    {
      return m_filename;
    }
    
    inline std::string Zglv::getFilename1() const
    {
      return m_filename1;
    }
    
    inline void Zglv::setVersion(const std::string &version)
    {
      m_version = version;
    }
    
    void Zglv::setData(const sphere::type::Date &data)
    {
      m_data = data;
    }
    
    inline void Zglv::setFilename(const std::string &filename)
    {
      m_filename = filename;
    }
    
    inline void Zglv::setFilename1(const std::string &filename1)
    {
      m_filename1 = filename1;
    }

  }
}
