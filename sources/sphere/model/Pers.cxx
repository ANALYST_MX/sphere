#include "model/Pers.hxx"

namespace sphere
{
  namespace model
  {

    Pers::~Pers()
    {
    }
  
    inline std::string Pers::getIdPac() const
    {
      return m_idPac;
    }
  
    inline std::string Pers::getFam() const
    {
      return m_fam;
    }
  
    inline std::string Pers::getIm() const
    {
      return m_im;
    }
  
    inline std::string Pers::getOt() const
    {
      return m_ot;
    }
  
    inline int Pers::getW() const
    {
      return m_w;
    }
  
    sphere::type::Date Pers::getDr() const
    {
      return m_dr;
    }
  
    inline int Pers::getDost() const
    {
      return m_dost;
    }
  
    inline std::string Pers::getFamP() const
    {
      return m_famP;
    }
  
    inline std::string Pers::getImP() const
    {
      return m_imP;
    }
  
    inline std::string Pers::getOtP() const
    {
      return m_otP;
    }
  
    inline int Pers::getWP() const
    {
      return m_wP;
    }
  
    sphere::type::Date Pers::getDrP() const
    {
      return m_drP;
    }
  
    inline int Pers::getDostP() const
    {
      return m_dostP;
    }
  
    inline std::string Pers::getMr() const
    {
      return m_mr;
    }
  
    inline std::string Pers::getDoctype() const
    {
      return m_doctype;
    }
  
    inline std::string Pers::getDocser() const
    {
      return m_docser;
    }
  
    inline std::string Pers::getDocnum() const
    {
      return m_docnum;
    }
  
    inline std::string Pers::getSnils() const
    {
      return m_snils;
    }
  
    inline int Pers::getStatus() const
    {
      return m_status;
    }
  
    inline std::string Pers::getOksm() const
    {
      return m_oksm;
    }
  
    inline std::string Pers::getOkatog() const
    {
      return m_okatog;
    }
  
    inline std::string Pers::getOkatop() const
    {
      return m_okatop;
    }
  
    inline int Pers::getZip() const
    {
      return m_zip;
    }
  
    inline std::string Pers::getArea() const
    {
      return m_area;
    }
  
    inline std::string Pers::getRegion() const
    {
      return m_region;
    }
  
    inline std::string Pers::getRegCity() const
    {
      return m_regCity;
    }
  
    inline std::string Pers::getItem() const
    {
      return m_item;
    }
  
    inline int Pers::getTypeItem() const
    {
      return m_typeItem;
    }
  
    inline int Pers::getTypeUl() const
    {
      return m_typeUl;
    }
  
    inline std::string Pers::getStreet() const
    {
      return m_street;
    }
  
    inline int Pers::getHouse() const
    {
      return m_house;
    }
  
    inline std::string Pers::getLiter() const
    {
      return m_liter;
    }
  
    inline std::string Pers::getFlat() const
    {
      return m_flat;
    }
  
    inline std::string Pers::getComentp() const
    {
      return m_comentp;
    }
  
    inline void Pers::setIdPac(const std::string &idPac)
    {
      m_idPac = idPac;
    }
  
    inline void Pers::setFam(const std::string &fam)
    {
      m_fam = fam;
    }
  
    inline void Pers::setIm(const std::string &im)
    {
      m_im = im;
    }
  
    inline void Pers::setOt(const std::string &ot)
    {
      m_ot = ot;
    }
  
    inline void Pers::setW(int w)
    {
      m_w = w;
    }
  
    void Pers::setDr(const sphere::type::Date &dr)
    {
      m_dr = dr;
    }
  
    inline void Pers::setDost(int dost)
    {
      m_dost = dost;
    }
  
    inline void Pers::setFamP(const std::string &famP)
    {
      m_famP = famP;
    }
  
    inline void Pers::setImP(const std::string &imP)
    {
      m_imP = imP;
    }
  
    inline void Pers::setOtP(const std::string &otP)
    {
      m_otP = otP;
    }
  
    inline void Pers::setWP(int wP)
    {
      m_wP = wP;
    }
  
    void Pers::setDrP(const sphere::type::Date &drP)
    {
      m_drP = drP;
    }
  
    inline void Pers::setDostP(int dostP)
    {
      m_dostP = dostP;
    }
  
    inline void Pers::setMr(const std::string &mr)
    {
      m_mr = mr;
    }
  
    inline void Pers::setDoctype(const std::string &doctype)
    {
      m_doctype = doctype;
    }
  
    inline void Pers::setDocser(const std::string &docser)
    {
      m_docser = docser;
    }
  
    inline void Pers::setDocnum(const std::string &docnum)
    {
      m_docnum = docnum;
    }
  
    inline void Pers::setSnils(const std::string &snils)
    {
      m_snils = snils;
    }
  
    inline void Pers::setStatus(int status)
    {
      m_status = status;
    }
  
    inline void Pers::setOksm(const std::string &oksm)
    {
      m_oksm = oksm;
    }
  
    inline void Pers::setOkatog(const std::string &okatog)
    {
      m_okatog = okatog;
    }
    
    inline void Pers::setOkatop(const std::string &okatop)
    {
      m_okatop = okatop;
    }
  
    inline void Pers::setZip(int zip)
    {
      m_zip = zip;
    }
  
    inline void Pers::setArea(const std::string &area)
    {
      m_area = area;
    }
  
    inline void Pers::setRegion(const std::string &region)
    {
      m_region = region;
    }
  
    inline void Pers::setRegCity(const std::string &regCity)
    {
      m_regCity = regCity;
    }
  
    inline void Pers::setItem(const std::string &item)
    {
      m_item = item;
    }
  
    inline void Pers::setTypeItem(int typeItem)
    {
      m_typeItem = typeItem;
    }
  
    inline void Pers::setTypeUl(int typeUl)
    {
      m_typeUl = typeUl;
    }

    inline void Pers::setStreet(const std::string &street)
    {
      m_street = street;
    }
  
    inline void Pers::setHouse(int house)
    {
      m_house = house;
    }
  
    inline void Pers::setLiter(const std::string &liter)
    {
      m_liter = liter;
    }
  
    inline void Pers::setFlat(const std::string &flat)
    {
      m_flat = flat;
    }
  
    inline void Pers::setComentp(const std::string &comentp)
    {
      m_comentp = comentp;
    }

  }
}
