#include "model/Sluch.hxx"

#include <algorithm>

namespace sphere
{
  namespace model
  {

    Sluch::~Sluch()
    {
    }
    
    inline std::string Sluch::getIdcase() const
    {
      return m_idcase;
    }
    
    inline int Sluch::getUslOk() const
    {
      return m_uslOk;
    }
    
    inline int Sluch::getVidpom() const
    {
      return m_vidpom;
    }
    
    inline int Sluch::getForPom() const
    {
      return m_forPom;
    }
    
    inline std::string Sluch::getVidHmp() const
    {
      return m_vidHmp;
    }
    
    inline int Sluch::getMetodHmp() const
    {
      return m_metodHmp;
    }
    
    inline std::string Sluch::getNprMo() const
    {
      return m_nprMo;
    }
    
    inline int Sluch::getExtr() const
    {
      return m_extr;
    }
    
    inline std::string Sluch::getLpu() const
    {
      return m_lpu;
    }
    
    inline std::string Sluch::getLpu1() const
    {
      return m_lpu1;
    }
    
    inline int Sluch::getLpuDep() const
    {
      return m_lpuDep;
    }
    
    inline int Sluch::getAmbDep() const
    {
      return m_ambDep;
    }
    
    inline int Sluch::getProfil() const
    {
      return m_profil;
    }
    
    inline int Sluch::getDet() const
    {
      return m_det;
    }
    
    inline std::string Sluch::getNhistory() const
    {
      return m_nhistory;
    }
    
    sphere::type::Date Sluch::getDate1() const
    {
      return m_date1;
    }
    
    sphere::type::Date Sluch::getDate2() const
    {
      return m_date2;
    }
    
    inline std::string Sluch::getDs0() const
    {
      return m_ds0;
    }
    
    inline std::string Sluch::getDs1() const
    {
      return m_ds1;
    }
    
    inline std::string Sluch::getDs2() const
    {
      return m_ds2;
    }
    
    inline std::string Sluch::getDs3() const
    {
      return m_ds3;
    }
    
    inline int Sluch::getVnovM() const
    {
      return m_vnovM;
    }
    
    inline std::string Sluch::getCodeMes1() const
    {
      return m_codeMes1;
    }
    
    inline std::string Sluch::getCodeMes2() const
    {
      return m_codeMes2;
    }
    
    inline int Sluch::getRslt() const
    {
      return m_rslt;
    }
    
    inline int Sluch::getIshod() const
    {
      return m_ishod;
    }
    
    inline int Sluch::getPrvs() const
    {
      return m_prvs;
    }
    
    inline std::string Sluch::getIddokt() const
    {
      return m_iddokt;
    }
    
    inline int Sluch::getIdsp() const
    {
      return m_idsp;
    }
    
    inline double Sluch::getEdCol() const
    {
      return m_edCol;
    }
    
    inline int Sluch::getReanD() const
    {
      return m_reanD;
    }
    
    inline double Sluch::getKskpCoef() const
    {
      return m_kskpCoef;
    }
    
    inline int Sluch::getKpg() const
    {
      return m_kpg;
    }
    
    inline int Sluch::getKsg() const
    {
      return m_ksg;
    }
    
    sphere::type::Money Sluch::getTarifR() const
    {
      return m_tarifR;
    }
    
    sphere::type::Money Sluch::getTarifF() const
    {
      return m_tarifF;
    }
    
    sphere::type::Money Sluch::getSumv() const
    {
      return m_sumvR;
    }
    
    sphere::type::Money Sluch::getSumvR() const
    {
      return m_sumvR;
    }
    
    sphere::type::Money Sluch::getSumvF() const
    {
      return m_sumvF;
    }
    
    inline int Sluch::getOplata() const
    {
      return m_oplata;
    }
    
    sphere::type::Money Sluch::getSump() const
    {
      return m_sump;
    }
    
    sphere::type::Money Sluch::getSumpR() const
    {
      return m_sumpR;
    }
    
    sphere::type::Money Sluch::getSumpF() const
    {
      return m_sumpF;
    }
    
    sphere::type::Money Sluch::getSankIt() const
    {
      return m_sankIt;
    }

    std::vector<std::shared_ptr<Sank> > &Sluch::getSank()
    {
      return m_sank;
    }
    
    const std::vector<std::shared_ptr<Sank> > &Sluch::getSank() const
    {
      return m_sank;
    }
    
    inline std::string Sluch::getComents() const
    {
      return m_coments;
    }
    
    inline int Sluch::getOsSluch() const
    {
      return m_osSluch;
    }

    std::vector<std::shared_ptr<Usl> > &Sluch::getUsl()
    {
      return m_usl;
    }
    
    const std::vector<std::shared_ptr<Usl> > &Sluch::getUsl() const
    {
      return m_usl;
    }
    
    inline int Sluch::getRefreason() const
    {
      return m_refreason;
    }
    
    inline void Sluch::setIdcase(const std::string &idcase)
    {
      m_idcase = idcase;
    }
    
    inline void Sluch::setUslOk(int uslOk)
    {
      m_uslOk = uslOk;
    }
    
    inline void Sluch::setVidpom(int vidpom)
    {
      m_vidpom = vidpom;
    }
    
    inline void Sluch::setForPom(int forPom)
    {
      m_forPom = forPom;
    }
    
    inline void Sluch::setVidHmp(const std::string &vidHmp)
    {
      m_vidHmp = vidHmp;
    }
    
    inline void Sluch::setMetodHmp(int metodHmp)
    {
      m_metodHmp = metodHmp;
    }
    
    inline void Sluch::setNprMo(const std::string &nprMo)
    {
      m_nprMo = nprMo;
    }
    
    inline void Sluch::setExtr(int extr)
    {
      m_extr = extr;
    }
    
    inline void Sluch::setLpu(const std::string &lpu)
    {
      m_lpu = lpu;
    }
    
    inline void Sluch::setLpu1(const std::string &lpu1)
    {
      m_lpu1 = lpu1;
    }
    
    inline void Sluch::setLpuDep(int lpuDep)
    {
      m_lpuDep = lpuDep;
    }
    
    inline void Sluch::setAmbDep(int ambDep)
    {
      m_ambDep = ambDep;
    }
    
    inline void Sluch::setProfil(int profil)
    {
      m_profil = profil;
    }
    
    inline void Sluch::setDet(int det)
    {
      m_det = det;
    }
    
    inline void Sluch::setNhistory(const std::string &nhistory)
    {
      m_nhistory = nhistory;
    }
    
    inline void Sluch::setDate1(const sphere::type::Date &date1)
    {
      m_date1 = date1;
    }
    
    inline void Sluch::setDate2(const sphere::type::Date &date2)
    {
      m_date2 = date2;
    }
    
    inline void Sluch::setDs0(const std::string &ds0)
    {
      m_ds0 = ds0;
    }
    
    inline void Sluch::setDs1(const std::string &ds1)
    {
      m_ds1 = ds1;
    }
    
    inline void Sluch::setDs2(const std::string &ds2)
    {
      m_ds2 = ds2;
    }
    
    inline void Sluch::setDs3(const std::string &ds3)
    {
      m_ds3 = ds3;
    }
    
    inline void Sluch::setVnovM(int vnovM)
    {
      m_vnovM = vnovM;
    }
    
    inline void Sluch::setCodeMes1(const std::string &codeMes1)
    {
      m_codeMes1 = codeMes1;
    }
    
    inline void Sluch::setCodeMes2(const std::string &codeMes2)
    {
      m_codeMes2 = codeMes2;
    }
    
    inline void Sluch::setRslt(int rslt)
    {
      m_rslt = rslt;
    }
    
    inline void Sluch::setIshod(int ishod)
    {
      m_ishod = ishod;
    }
    
    inline void Sluch::setPrvs(int prvs)
    {
      m_prvs = prvs;
    }
    
    inline void Sluch::setIddokt(const std::string &iddokt)
    {
      m_iddokt = iddokt;
    }
    
    inline void Sluch::setIdsp(int idsp)
    {
      m_idsp = idsp;
    }
    
    inline void Sluch::setEdCol(double edCol)
    {
      m_edCol = edCol;
    }
    
    inline void Sluch::setReanD(int reanD)
    {
      m_reanD = reanD;
    }
    
    inline void Sluch::setKskpCoef(double kskpCoef)
    {
      m_kskpCoef = kskpCoef;
    }
    
    inline void Sluch::setKpg(int kpg)
    {
      m_kpg = kpg;
    }
    
    inline void Sluch::setKsg(int ksg)
    {
      m_ksg = ksg;
    }
    
    void Sluch::setTarifR(const sphere::type::Money &tarifR)
    {
      m_tarifR = tarifR;
    }
    
    void Sluch::setTarifF(const sphere::type::Money &tarifF)
    {
      m_tarifF = tarifF;
    }
    
    void Sluch::setSumv(const sphere::type::Money &sumv)
    {
      m_sumv = sumv;
    }
    
    void Sluch::setSumvR(const sphere::type::Money &sumvR)
    {
      m_sumvR = sumvR;
    }
    
    void Sluch::setSumvF(const sphere::type::Money &sumvF)
    {
      m_sumvF = sumvF;
    }
    
    inline void Sluch::setOplata(int oplata)
    {
      m_oplata = oplata;
    }
    
    void Sluch::setSump(const sphere::type::Money &sump)
    {
      m_sump = sump;
    }
    
    void Sluch::setSumpR(const sphere::type::Money &sumpR)
    {
      m_sumpR = sumpR;
    }
    
    void Sluch::setSumpF(const sphere::type::Money &sumpF)
    {
      m_sumpF = sumpF;
    }
    
    void Sluch::setSankIt(const sphere::type::Money &sankIt)
    {
      m_sankIt = sankIt;
    }
    
    bool Sluch::addSank(std::shared_ptr<Sank> sank)
    {
      auto result{ false };
      m_sank.emplace_back(sank);
      result = true;

      return result;
    }
    
    bool Sluch::removeSank(std::shared_ptr<Sank> sank)
    {
      auto result{ false };
      m_sank.erase(std::remove_if(m_sank.begin(), m_sank.end(), [&](std::shared_ptr<Sank> found)
				  {
				    result = found == sank;
				    return result;
				  }
				  ), m_sank.end());

      return result;
    }
    
    inline void Sluch::setComents(const std::string &coments)
    {
      m_coments = coments;
    }
    
    inline void Sluch::setOsSluch(int osSluch)
    {
      m_osSluch = osSluch;
    }
    
    bool Sluch::addUsl(std::shared_ptr<Usl> usl)
    {
      auto result{ false };
      m_usl.emplace_back(usl);
      result = true;

      return result;
    }
    
    bool Sluch::removeUsl(std::shared_ptr<Usl> usl)
    {
      auto result{ false };
      m_usl.erase( std::remove_if(m_usl.begin(), m_usl.end(), [&](std::shared_ptr<Usl> found)
				  {
				    result = found == usl;
				    return result;
				  }
				  ), m_usl.end());
      
      return result;
    }
    
    inline void Sluch::setRefreason(int refreason)
    {
      m_refreason = refreason;
    }    
    
  }
}

