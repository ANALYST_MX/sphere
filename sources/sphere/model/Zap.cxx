#include "model/Zap.hxx"

namespace sphere
{
  namespace model
  {

    Zap::~Zap()
    {
    }
    
    inline int Zap::getNZap() const
    {
      return m_nZap;
    }
    
    inline int Zap::getPrNov() const
    {
      return m_prNov;
    }
    
    std::shared_ptr<Pacient> Zap::getPacient() const  
    {
      return m_pacient;
    }
    
    std::shared_ptr<Sluch> Zap::getSluch() const
    {
      return m_sluch;
    }
    
    inline void Zap::setNZap(int nZap)
    {
      m_nZap = nZap;
    }
    
    inline void Zap::setPrNov(int prNov)
    {
      m_prNov = prNov;
    }
    
    void Zap::setPacient(std::shared_ptr<Pacient> pacient)
    {
      m_pacient = pacient;
    }
    
   void Zap::setSluch(std::shared_ptr<Sluch> sluch)
    {
      m_sluch = sluch;
    }

  }
}
