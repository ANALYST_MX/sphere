#include "type/Money.hxx"

#include <cmath>
#include <string>

namespace sphere
{
  namespace type
  {

    Money::Money(const Money &other)
    {
      m_currency = new BigInteger(*other.m_currency);
    }

    Money::Money(const double &val)
    {
      m_currency = new BigInteger(static_cast<long long>(val * BASE));
    }
    
    Money::~Money()
    {
      delete m_currency;
    }

    Money::Money(const BigInteger &currency)
    {
      m_currency = new BigInteger(currency);
    }

    double Money::getValue() const
    {
      return static_cast<double>(std::stoll(m_currency->toString())) / BASE;
    }

    Money Money::operator+(const Money &other) const
    {
      return Money(*m_currency + *other.m_currency);
    }
    
    Money Money::operator-(const Money &other) const
    {
      return Money(*m_currency - *other.m_currency);
    }
    
    Money Money::operator*(const Money &other) const
    {
      return Money(*m_currency * *other.m_currency);
    }
    
    Money Money::operator/(const Money &other) const
    {
      return Money(*m_currency / *other.m_currency);
    }

    bool Money::operator==(const Money &other) const
    {
      return *m_currency == *other.m_currency;
    }
  }
}
