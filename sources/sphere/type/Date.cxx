#include "type/Date.hxx"

#include <vector>
#include <algorithm>

using namespace std::rel_ops;

namespace sphere
{
  namespace type
  {

    Date::Date(const int &day, const int &month, const int &year)
    {
      m_day = day;
      m_month = month;
      m_year = year;
    }

    Date::Date(const Date &d)
    {
      m_day = d.m_day;
      m_month = d.m_month;
      m_year = d.m_year;
    }
    
    Date::~Date()
    {
    }

    Date Date::nextDate(const Date &d)
    {
      if (!d.isValid())
	{
	  return Date();
	}
      Date ndat{ d };
      std::vector<int> monthsWith31 = { 1, 3, 5, 7, 8, 10, 12 };
      if (ndat.m_day == 31)
	{
	  ndat.m_day = 1;
	  ndat.m_month++;
	}
      else if (ndat.m_day == 30 &&
	       std::find(monthsWith31.begin(),
			 monthsWith31.end(), ndat.m_month) == monthsWith31.end())
	{
	  ndat.m_day = 1;
	  ndat.m_month++;
	}
      else if (ndat.m_day == 29 && ndat.m_month == 2)
	{
	  ndat.m_day = 1;
	  ndat.m_month++;
	}
      else if (ndat.m_day == 28 && ndat.m_month == 2  && !ndat.isLeapYear())
	{
	  ndat.m_day = 1;
	  ndat.m_month++;
	}
      else
	{
	  ndat.m_day++;
	}

      if (ndat.m_month > 12)
	{
	  ndat.m_month = 1;
	  ndat.m_year++;
	}
      
      return ndat;
    }
    
    Date Date::previousDate(const Date &d)
    {
      if (!d.isValid())
	{
	  return Date();
	}
      Date pdat{ d };
      std::vector<int> monthsWith30 = { 4, 6, 9, 11 };
      if (pdat.m_day == 1)
	{
	  pdat.m_month--;
	}

      if (pdat.m_day == 1 && std::find(monthsWith30.begin(), monthsWith30.end(), pdat.m_month) != monthsWith30.end())
	{
	  pdat.m_day = 30;
	}
      else if (pdat.m_day == 1 && pdat.m_month == 2 && pdat.isLeapYear())
	{
	  pdat.m_day = 29;
	}
      else if (pdat.m_day == 1 && pdat.m_month == 2)
	{
	  pdat.m_day = 28;
	}
      else if (pdat.m_day == 1)
	{
	  pdat.m_day = 31;
	}
      else
	{
	  pdat.m_day--;
	}

      if (pdat.m_month < 1)
	{
	  pdat.m_month = 12;
	  pdat.m_year--;
	}

      return pdat;
    }
    
    bool Date::isValid() const
    {
      if (m_year < 0) return false;
      if (m_month > 12 || m_month < 1) return false;
      if (m_day > 31 || m_day < 1) return false;
      if ((m_day == 31 && ( m_month ==2 || m_month == 4 || m_month == 6 || m_month == 9 || m_month == 11))) return false;
      if (m_day == 30 && m_month == 2) return false;
      
      return true;
    }
    
    inline int Date::getDay() const
    {
      return m_day;
    }
    
    inline int Date::getMonth() const
    {
      return m_month;
    }
    
    inline int Date::getYear() const
    {
      return m_year;
    }
    
    inline void Date::setDay(const int &day)
    {
      m_day = day;
    }
    
    inline void Date::setMonth(const int &month)
    {
      m_month = month;
    }
    
    inline void Date::setYear(const int &year)
    {
      m_year = year;
    }

    sphere::type::Date::DayOfWeek Date::getDayOfWeek() const
    {
      int a = (14 - m_month) / 12;
      int y = m_year - a;
      int m = m_month + 12 * a - 2;
      int day = (7000 + (m_day + y + y / 4 - y / 100 + y / 400 + (31 * m) / 12)) % 7;
      return (sphere::type::Date::DayOfWeek) day;
    }
    
    Date &Date::operator=(const Date &d)
    {
      m_year = d.m_year;
      m_month = d.m_month;
      m_day = d.m_day;
      
      return *this;
    }
    
    Date &Date::operator++()
    {
      *this = nextDate(*this);
      return *this;
    }

    Date Date::operator++(int)
    {
      Date d = *this;
      *this = nextDate(d);
      return d;
    }
    
    Date &Date::operator--()
    {
      *this = previousDate(*this);
      return *this;
    }
    
    Date Date::operator--(int)
    {
      Date d = *this;
      *this = previousDate(*this);
      return d;
    }
    
    bool Date::operator==(const Date &d) const
    {
      return m_year == d.m_year && m_month == d.m_month && m_day == d.m_day;
    }
    
    bool Date::operator<(const Date &d) const
    {
      if (m_year < d.m_year) return true;
      if (m_year == d.m_year && m_month < d.m_month) return true;
      if (m_year == d.m_year && m_month == d.m_month && m_day < d.m_day) return true;
      return false;
    }
    
    bool Date::isLeapYear() const
    {
      if (m_year % 4 != 0) return false;
      if (m_year % 100 != 0) return true;
      if (m_year % 400 != 0) return false;
      return true;
    }
      
  }
}
