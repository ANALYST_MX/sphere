#include "type/BigInteger.hxx"

#include <algorithm>
#include <iterator>
#include <sstream>

namespace sphere
{
  namespace type
  {

    BigInteger::BigInteger()
    {
      m_negative = false;
    }

    BigInteger::BigInteger(const BigInteger &number)
    {
      m_value = number.m_value;
      m_negative = number.m_negative;
    }

    BigInteger::BigInteger(const long long int number)
    {
      if (number > -10 && number < 10)
	{
	  m_negative = number < 0;
	  m_value.push_back(static_cast<uint8_t>(m_negative ? -1 * number : number));
	}
      else
	setNumber(std::to_string(number));
    }
    
    BigInteger::BigInteger(const std::string &str)
    {
      setNumber(str);
    }

    BigInteger::~BigInteger()
    {
      m_value.clear();
    }

    int compare(const std::vector<uint8_t> &left, const std::vector<uint8_t> &right)
    {
      if (left.size() != right.size())
	return left.size() - right.size();
      else
	{
	  for (auto leftIt = left.crbegin(), rightIt = right.crbegin(); leftIt != left.crend(); ++leftIt, ++rightIt)
	    {
	      if (!(*leftIt == *rightIt))
		{
		  return *leftIt - *rightIt;
		}
	    }
	}
      return 0;
    }

    void BigInteger::setNumber(const std::string &str)
    {
      auto end = str.crend();
      if (isdigit(str[0]))
	{
	  m_negative = false;
	}
      else
	{
	  m_negative = (str[0] == '-');
	  --end;
	}
      std::transform(str.crbegin(), end, std::back_inserter(m_value), toUInt8);
    }

    BigInteger &BigInteger::operator=(const BigInteger &number)
    {
      if (this == &number)
	return *this;
      m_value = number.m_value;
      m_negative = number.m_negative;
      return *this;
    }

    BigInteger &BigInteger::operator=(const long long int number)
    {
      if (number > -10 && number < 10)
	{
	  m_negative = number < 0;
	  m_value.push_back(static_cast<uint8_t>(m_negative ? -1 * number : number));
	}
      else
	setNumber(std::to_string(number));
      return *this;
    }

    BigInteger &BigInteger::operator=(const std::string &str)
    {
      setNumber(str);
      return *this;
    }

    bool BigInteger::operator==(const BigInteger &other) const
    {
      if (m_negative != other.m_negative) return false;
      if (compare(m_value, other.m_value) == 0)
	  return true;
      else
	  return false;
    }

    bool BigInteger::operator<(const BigInteger &other) const
    {
      auto less{ false };
      if (m_negative != other.m_negative)
	{
	  if (m_negative == true) less = true;
	}
      else if (m_negative == true)
	{
	  if (compare(m_value, other.m_value) > 0) less = true;
	}
      else if (m_negative == false)
	{
	  if (compare(m_value, other.m_value) < 0) less = true;
	}
      return less;
    }

    std::ostream &operator<<(std::ostream &out, const BigInteger &number)
    {
      if (number.m_negative)
	out << "-";
      std::transform(number.m_value.crbegin(), number.m_value.crend(), std::ostream_iterator<uint8_t>(out, ""), sphere::type::BigInteger::toChar);
      return out;
    }

    std::vector<uint8_t> BigInteger::add(const std::vector<uint8_t> &lnumber, const std::vector<uint8_t> &rnumber)
    {
      std::vector<uint8_t> result;
      auto carry{ false };
      size_t max_size = std::max(lnumber.size(), rnumber.size());
      for (size_t i{ 0 }, n{ 0 }; i != max_size; ++i)
	{
	  n = (i < lnumber.size() ? lnumber[i] : 0) + (i < rnumber.size() ? rnumber[i] : 0) + (carry ? 1 : 0);
	  if (n >= 10)
	    {
	      carry = true;
	      n %= 10;
	    }
	  else
	    carry = false;
	  result.push_back(n);
	}
      if (carry) result.push_back(1);
      return result;
    }
    
    std::vector<uint8_t> BigInteger::sub(const std::vector<uint8_t> &lnumber, const std::vector<uint8_t> &rnumber)
    {
      std::vector<uint8_t> result;
      if (compare(lnumber, rnumber) == 0)
	{
	  result.push_back(0);
	  return result;
	}
      for (struct { size_t i; int8_t top; bool borrow; } v = { 0, 0, false }; v.i < lnumber.size(); ++v.i)
	{
	  if (v.borrow)
	    {
	      v.top = lnumber[v.i] - 1;
	      v.borrow = false;
	    }
	  else
	    v.top = lnumber[v.i];

	  if (rnumber.size() > v.i) v.top -= rnumber[v.i]; 
	  if (v.top < 0)
	    {
	      v.borrow = true;
	      v.top += 10;
	    }
	  result.push_back(v.top);
	}
      if (result.back() == 0) result.pop_back();
      return result;
    }
    
    std::vector<uint8_t> BigInteger::mul(const std::vector<uint8_t> &lnumber, const std::vector<uint8_t> &rnumber)
    {
      std::vector<uint8_t> result(lnumber.size() + rnumber.size(), 0);
      for (size_t i{ 0 }, j{ 0 }; i != lnumber.size(); ++i)
	{
	  j = i;
	  for (size_t g{ 0 }; g != rnumber.size(); g++, j++)
	    {
	      result[j] += lnumber[i] * rnumber[g];
	      result[j + 1] += result[j] / 10;
	      result[j] %= 10;
	    }
	    }
      auto i = result.end() - 1;
      if (result.size() > 1 && *i == 0)
	  i = result.erase(i) - 1;
      return result;
    }
    
    std::vector<uint8_t> BigInteger::div(const std::vector<uint8_t> &lnumber, const std::vector<uint8_t> &rnumber)
    {
      std::vector<uint8_t> result{ 0 };
      if (compare(lnumber, rnumber) < 0)
	{
	  result.push_back(0);
	  return result;
	}
      std::vector<uint8_t> l = lnumber, r = rnumber;
      size_t x{ 0 };
      while (compare(l, r) >= 0)
	{
	  r.insert(r.cbegin(), 0);
	  ++x;
	}
      r.erase(r.cbegin());
      std::vector<uint8_t> one = { 1 };
      while (x--)
	{
	  while (compare(l, r) >= 0)
	    {
	      l = sub(l,  r);
	      result = add(result, one);
	    }
	  result.insert(result.cbegin(), 0);
	  r.erase(r.cbegin());
	}
      result.erase(result.cbegin());
      if (result.size() == 0) result.push_back(0);
      
      return result;
    }
    
    std::vector<uint8_t> BigInteger::mod(const std::vector<uint8_t> &lnumber, const std::vector<uint8_t> &rnumber)
    {
      if (compare(lnumber, rnumber) < 0) return lnumber;
      size_t x{ 0 };
      std::vector<uint8_t> l = lnumber, r = rnumber;
      while (compare(l, r) >= 0)
	{
	  r.insert(r.cbegin(), 0);
	  x++;
	}
      r.erase(r.cbegin());
      while (x--)
	{
	  while (compare(l, r) >= 0) l = sub(l, r);
	  r.erase(r.cbegin());
	}
      return l;
    }

    BigInteger BigInteger::operator+(const BigInteger &rnumber) const
    {
      BigInteger result;
      if (m_negative == rnumber.m_negative)
	{
	  result.m_negative = m_negative;
	  result.m_value = add(m_value, rnumber.m_value);
	}
      else
	{
	  if (compare(m_value, rnumber.m_value) >= 0)
	    {
	      result.m_negative = m_negative;
	      result.m_value = sub(m_value, rnumber.m_value);
	    }
	  else
	    {
	      result.m_negative = rnumber.m_negative;
	      result.m_value = sub(rnumber.m_value, m_value);
	    }
	}
      return result;
    }
    
    BigInteger BigInteger::operator-(const BigInteger &rnumber) const
    {
      BigInteger result;
      if (m_negative != rnumber.m_negative)
	{
	  result.m_negative = m_negative;
	  result.m_value = add(m_value, rnumber.m_value);
	}
      else
	{
	  if (compare(m_value, rnumber.m_value) == 0)
	    {
	      result = 0;
	    }
	  else if (compare(m_value, rnumber.m_value) > 0)
	    {
	      result.m_negative = m_negative;
	      result.m_value = sub(m_value, rnumber.m_value);
	    }
	  else
	    {
	      result.m_negative = !rnumber.m_negative;
	      result.m_value = sub(rnumber.m_value, m_value);
	    }
	}
      return result;
    }
    
    BigInteger BigInteger::operator*(const BigInteger &rnumber) const
    {
      BigInteger result;
      if (m_negative != rnumber.m_negative) result.m_negative = true;
      result.m_value = mul(m_value, rnumber.m_value);
      
      return result;
    }
    
    BigInteger BigInteger::operator/(const BigInteger &rnumber) const throw(sphere::exception::Exception)
    {
      BigInteger zero(0);
      if (rnumber == zero) throw sphere::exception::Exception("Division by zero");
      if (compare(m_value, rnumber.m_value) <= 0) return zero;

      BigInteger result;
      if (m_negative != rnumber.m_negative) result.m_negative = true;
      
      result.m_value = div(m_value, rnumber.m_value);
      return result;
    }
     
    BigInteger BigInteger::operator%(const BigInteger &rnumber) const
    {
      BigInteger result;
      result.m_value = mod(m_value, rnumber.m_value);
      return result;
    }

    BigInteger &BigInteger::operator++()
    {
      *this = *this + BigInteger(1);
      return *this;
    }
    
    BigInteger BigInteger::operator++(int)
    {
      BigInteger result(*this);
      *this = *this + BigInteger(1);
      return result;
    }
    
    BigInteger &BigInteger::operator--()
    {
      *this = *this - BigInteger(1);
      return *this;
    }
    
    BigInteger BigInteger::operator--(int)
    {
      BigInteger result(*this);
      *this = *this - BigInteger(1);
      return result;
    }

    BigInteger BigInteger::operator-() const
    {
      BigInteger result = *this;
      result.m_negative = !result.m_negative;
      return result;
    }

    std::string BigInteger::toString() const
    {
      std::string result;
      if (m_negative)
	result = "-";
      std::transform(m_value.crbegin(), m_value.crend(), std::back_inserter(result), toChar);
      return result;
    }
    
    inline uint8_t BigInteger::toUInt8(char c)
    {
      return static_cast<uint8_t>(c) - static_cast<uint8_t>('0');
    }

    inline char BigInteger::toChar(uint8_t n)
    {
      return static_cast<char>(n + '0');
    }

  }
}
