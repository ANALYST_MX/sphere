#include "util/FileManager.hxx"

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fstream>
#include <algorithm>

#include "exception/Exception.hxx"

namespace sphere
{
  namespace util
  {

    FileManager::FileManager(const std::string &basePath)
      : m_basePath(basePath)
    {
    }

    FileManager::FileManager(const FileManager &v)
      : m_basePath(v.m_basePath)
    {
    }

    FileManager::~FileManager()
    {

    }

    bool FileManager::isDirectory(const std::string &dirPath)
    {
      struct stat info;
      if (0 != stat(dirPath.c_str(), &info))
	{
	  return false;
	}
      if (S_ISDIR(info.st_mode))
	{
	  return true;
	}
      else
	{
	  return false;
	}
    }

    bool FileManager::isRegularFile(const std::string &filename)
    {
      std::fstream f;
      f.open(filename.c_str(), std::ios::in);
      
      return f.is_open();
    }

    bool FileManager::isDirectoryExist(const std::string &dirPath)
    {
      return isDirectory(dirPath);
    }

    bool FileManager::createDirectory(const std::string &dirPath)
    {
      return mkdir(dirPath.c_str(), S_IRWXU | S_IRGRP | S_IWGRP) == 0;
    }

    std::string FileManager::normalizePath(const std::string &path)
    {
      std::string cleanstr;
      char *clean = realpath(path.c_str(), nullptr);
      if (clean)
	{
	  cleanstr = std::string(clean);
	}
      else
	{
	  cleanstr = path;
	  while (*cleanstr.rbegin() == '/')
	    {
	      cleanstr.resize(cleanstr.size() - 1);
	    }
	}
      free(clean);
      return cleanstr;
    }

    FileManager &FileManager::operator=(const FileManager &v)
    {
      if (this != &v)
	{
	  m_basePath = v.m_basePath;
	}
      
      return *this;
    }

    void FileManager::removeFiles()
    {
      std::vector<std::string> files = getDirectoryContents();
      for (auto file : files)
	{	  
	  if (isRegularFile(file))
	    {
	      std::remove(file.c_str()); 
	    }
	}
    }

    std::vector<std::string> FileManager::getDirectoryContents()
    {
      std::vector<std::string> entries;
      if (isDirectory(m_basePath))
	{
	  DIR *dir;
	  if ((dir = opendir(m_basePath.c_str())) == nullptr)
	    {
	      throw sphere::exception::Exception("Couldn't open directory \"%s\"", m_basePath.c_str());
	    }
	  struct dirent *ent;
	  while ((ent = readdir(dir)) != nullptr)
	    {
	      entries.push_back(ent->d_name);
	    }
	  closedir(dir);
	}
      return entries;
    }

    void FileManager::copyRegularFile(const std::string &src, const std::string &dst)
    {
      if (isRegularFile(src))
	{
	  std::ifstream source(src, std::ios::binary);
	  std::ofstream dest(dst, std::ios::binary);

	  std::istreambuf_iterator<char> begin_source(source);
	  std::istreambuf_iterator<char> end_source;
	  std::ostreambuf_iterator<char> begin_dest(dest);
	  std::copy(begin_source, end_source, begin_dest);

	  source.close();
	  dest.close();
	}
    }

    void FileManager::copyToDirectory(const std::string &tmpDir)
    {
      std::vector<std::string> files = getDirectoryContents();
      createDirectory(tmpDir);
      for (auto file : files)
	{
	  if (isRegularFile(file))
	    {
	      copyRegularFile(file, tmpDir + "/" + file);
	    }
	}      
    }

  }
}
