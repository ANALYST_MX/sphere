#include "util/file/Path.hxx"

#include <algorithm>
#include <stack>

namespace sphere
{
  namespace util
  {
    namespace file
    {

      const std::string Path::DOT(".");
      const std::string Path::DOT_DOT("..");
#ifdef _WIN32
      const char Path::PATH_SEPARATOR('\\');
#else
      const char Path::PATH_SEPARATOR('/');
#endif

      Path::Path(const Path &p)
	: m_path(p.m_path)
      {
      }
      
      Path::Path(Path &&p)
	: m_path(std::move(p.m_path))
      {
      }
      
      Path::Path(const std::string &filepath)
	: m_path(filepath)
      {
      }
      
      Path::Path(std::string &&filepath)
	: m_path(std::move(filepath))
      {
      }
      
      Path::~Path()
      {
      }
      
      Path &Path::operator=(const Path &p)
      {
	m_path = p.m_path;
	return *this;
      }
      
      Path &Path::operator=(const std::string &filepath)
      {
	m_path = filepath;
	return *this;
      }

      Path &Path::operator=(Path &&p)
      {
	m_path = std::move(p.m_path);
	return *this;
      }

      Path &Path::operator=(std::string &&filepath)
      {
	m_path = std::move(filepath);
	return *this;
      }

      Path::operator std::string() const
      {
	return m_path;
      }
      
      Path::operator const char *() const
      {
	return m_path.c_str();
      }

      Path &Path::concat(const std::string &p)
      {
	if (p.length() > 0 && m_path.length() > 0 &&
	    p.front() == PATH_SEPARATOR && m_path.back() == PATH_SEPARATOR)
	  m_path.append(p, 1, p.size() - 1);
	else
	  if (p.front() != PATH_SEPARATOR && m_path.back() != PATH_SEPARATOR)
	    m_path.append(sizeof(PATH_SEPARATOR), PATH_SEPARATOR).append(p);
	return *this;
      }

      Path Path::simplify(const Path &p)
      {
	std::string path{ p.m_path };
	std::stack<std::string> s;
	std::string r;
	size_t i{ 1 };
	while (i < path.size())
	  {
	    size_t j{ 0 };
	    while (i + j < path.size() && path[i + j] != PATH_SEPARATOR) ++j;
	    std::string t = path.substr(i, j);
	    if (t == DOT_DOT)
	      {
		if (!s.empty())
		  {
		    s.pop();
		    s.pop();
		  }
	      }
	    else if (t.empty() || t == DOT) {}
	    else
	      {
		s.push(std::string(sizeof(PATH_SEPARATOR), PATH_SEPARATOR));
		s.push(t);
	      }
	    i += j+1;
	  }
	if (s.empty() && path.size() != 0)
	  r = std::string(sizeof(PATH_SEPARATOR), PATH_SEPARATOR);
	else
	  while (!s.empty())
	    {
	      r = s.top() + r;
	      s.pop();
	    }
	return Path(r);
      }
    }
  }
}
