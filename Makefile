BUILD_TYPE = debug

CXX = .cxx
HXX = .hxx
O = .o

CC = c++
CXXFLAGS = -std=c++11
CXXFLAGS += -Wall
ifeq ($(BUILD_TYPE), debug)
	CXXFLAGS += -g
else
	CXXFLAGS += -O2 -w
endif

NAMESPACE = sphere
INCLUDESDIR = -iquoteincludes/$(NAMESPACE)
SOURCESDIR = sources/$(NAMESPACE)
BUILDDIR = build/$(NAMESPACE)
BINARY = build/target
MAINSOURCE = sources/Application$(CXX)

all: build
	$(CC) -iquoteincludes $(CXXFLAGS) $(MAINSOURCE) -o $(BINARY)

$(BUILDDIR)/%$(O): $(SOURCESDIR)/%$(CXX)
	$(CC) $(INCLUDESDIR) $(CXXFLAGS) -c $< -o $@

build:
	mkdir -p $(BUILDDIR)
	mkdir -p $(BUILDDIR)/{model,exception,util/file,type}

clean:
	rm -rf build
