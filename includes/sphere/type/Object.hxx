#pragma once

namespace sphere
{
  namespace type
  {
  
    class Object
    {
    public:
      Object() = default;
      virtual ~Object();
    };

  }
}
