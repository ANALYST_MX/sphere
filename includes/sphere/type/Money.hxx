#pragma once

#include "type/BigInteger.hxx"

namespace sphere
{
  namespace type
  {

    class Money
    {
    public:
      Money() = delete;
      Money(const Money &);
      Money(const double &);
      virtual ~Money();
      Money operator+(const Money &) const;
      Money operator-(const Money &) const;
      Money operator*(const Money &) const;
      Money operator/(const Money &) const;
      bool operator==(const Money &) const;
      double getValue() const;
    protected:
      Money(const BigInteger &);
    private:
      BigInteger *m_currency;
    private:
      static const unsigned int BASE;
    };

    const unsigned int Money::BASE = 100;
    
  }
}
