#pragma once

#include <vector>
#include <iostream>
#include <string>
#include <utility>

#include "exception/Exception.hxx"

namespace sphere
{
  namespace type
  {
    
    using std::rel_ops::operator!=;
    using std::rel_ops::operator>;
    using std::rel_ops::operator<=;
    using std::rel_ops::operator>=;
    
    class BigInteger
    {
      friend std::ostream &operator<<(std::ostream &, const BigInteger &);
    public:
      BigInteger();
      BigInteger(const BigInteger &);
      BigInteger(const long long int);
      BigInteger(const std::string &);
      virtual ~BigInteger();
      void setNumber(const std::string &);
      BigInteger &operator=(const BigInteger &);
      BigInteger &operator=(const long long int);
      BigInteger &operator=(const std::string &);
      bool operator==(const BigInteger &) const;
      bool operator<(const BigInteger &) const;
      BigInteger operator+(const BigInteger &) const;
      BigInteger operator-(const BigInteger &) const;
      BigInteger operator*(const BigInteger &) const;
      BigInteger operator/(const BigInteger &) const throw(sphere::exception::Exception);
      BigInteger operator%(const BigInteger &) const;
      BigInteger &operator++();
      BigInteger operator++(int);
      BigInteger &operator--();
      BigInteger operator--(int);
      BigInteger operator-() const;
      std::string toString() const;
    protected:
      static uint8_t toUInt8(char);
      static char toChar(uint8_t);
      static int cmp(const std::vector<uint8_t> &, const std::vector<uint8_t> &);
      static std::vector<uint8_t> add(const std::vector<uint8_t> &lnumber, const std::vector<uint8_t> &rnumber);
      static std::vector<uint8_t> sub(const std::vector<uint8_t> &lnumber, const std::vector<uint8_t> &rnumber);
      static std::vector<uint8_t> mul(const std::vector<uint8_t> &lnumber, const std::vector<uint8_t> &rnumber);
      static std::vector<uint8_t> div(const std::vector<uint8_t> &lnumber, const std::vector<uint8_t> &rnumber);
      static std::vector<uint8_t> mod(const std::vector<uint8_t> &lnumber, const std::vector<uint8_t> &rnumber);
    private:
      std::vector<uint8_t> m_value;
      bool m_negative;
    };
    
  }
}
