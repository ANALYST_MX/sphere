#pragma once

#include <string>
#include <utility>

namespace sphere
{
  namespace type
  {

    using std::rel_ops::operator!=;
    using std::rel_ops::operator>;
    using std::rel_ops::operator<=;
    using std::rel_ops::operator>=;

    class Date
    {
    public:
      enum class DayOfWeek : char { Mon = 0, Tue, Wed, Thu, Fri, Sat, Sun };
      
      Date() = default;
      Date(const int &, const int &, const int &);
      Date(const Date &);
      virtual ~Date();
      bool isValid() const;
      int getDay() const;
      int getMonth() const;
      int getYear() const;
      void setDay(const int &);
      void setMonth(const int &);
      void setYear(const int &);
      Date &operator=(const Date &);
      Date &operator++();
      Date operator++(int);
      Date &operator--();
      Date operator--(int);
      bool operator==(const Date &) const;
      bool operator<(const Date &) const;
      bool isLeapYear() const;
      DayOfWeek getDayOfWeek() const;
    protected:
      static Date nextDate(const Date &);
      static Date previousDate(const Date &);

    private:
      int m_year;
      int m_month;
      int m_day;
    };
  }
}
