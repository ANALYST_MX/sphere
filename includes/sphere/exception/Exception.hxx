#pragma once

#include <exception>
#include <string>

namespace sphere
{
  namespace exception
  {
    
    class Exception : public std::exception
    {
    public:
      Exception(const char *, ...) throw()
	__attribute__((format (__printf__, 2, 3)));
      Exception(const Exception &other) throw();
      virtual ~Exception() throw();
      virtual const char *what() const throw();
      virtual std::string toString() const;
      virtual char const *type() const;
    private:
      std::string m_message;
    };

  }
}
    
