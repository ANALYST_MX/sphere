#pragma once

#include <string>
#include <vector>

#include "Object.hxx"

namespace sphere
{
  namespace util
  {

    class FileManager
      : public Object
    {
    public:
      FileManager(const std::string &);
      FileManager(const FileManager &);
      ~FileManager();
      FileManager &operator=(const FileManager &);

      void removeFiles();
      std::vector<std::string> getDirectoryContents();
      void copyToDirectory(const std::string &);

    public:
      static bool isDirectoryExist(const std::string &);
      static void copyRegularFile(const std::string &, const std::string &);
      static bool createDirectory(const std::string &);
      static bool isDirectory(const std::string &);
      static bool isRegularFile(const std::string &);
      static std::string normalizePath(const std::string &);

    private:
      std::string m_basePath;
    };

  }
}
