#pragma once

#include <string>

namespace sphere
{
  namespace util
  {
    namespace file
    {
      
      class Path
      {
      public:
	Path() = default;
	Path(const Path &);
	Path(Path &&);
	Path(const std::string &);
	Path(std::string &&);
	virtual ~Path();
	Path &operator=(const Path &);
	Path &operator=(Path &&);
	Path &operator=(const std::string &);
	Path &operator=(std::string &&);
	Path &concat(const std::string &);
	operator std::string() const;
	operator const char *() const;
      public:
	static const char PATH_SEPARATOR;
	static const std::string DOT;
	static const std::string DOT_DOT;
	static Path simplify(const Path &);
      private:
	std::string m_path;
      };
      
    }
  }
}
