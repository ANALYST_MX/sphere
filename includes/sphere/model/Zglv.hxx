#pragma once

#include <string>

#include "type/Object.hxx"
#include "type/Date.hxx"

namespace sphere
{
  namespace model
  {

    class Zglv
      : public sphere::type::Object
    {
    public:
      Zglv() = default;
      virtual ~Zglv();
      
      std::string getVersion() const;
      sphere::type::Date getData() const;
      std::string getFilename() const;
      std::string getFilename1() const;
      void setVersion(const std::string &);
      void setData(const sphere::type::Date &);
      void setFilename(const std::string &);
      void setFilename1(const std::string &);
    private:
      std::string m_version;
      sphere::type::Date m_data;
      std::string m_filename;
      std::string m_filename1;
    };
    
  }
}
