#pragma once

#include <vector>
#include <memory>

#include "type/Object.hxx"
#include "model/Zglv.hxx"
#include "model/Pers.hxx"

namespace sphere
{
  namespace model
  {
    
    class PersList
      : public sphere::type::Object
    {
    public:
      PersList() = default;
      virtual ~PersList();
      std::shared_ptr<Zglv> getZglv() const;
      std::vector<std::shared_ptr<Pers> > &getPers();
      const std::vector<std::shared_ptr<Pers> > &getPers() const;
      void setZglv(std::shared_ptr<Zglv>);
      bool addPers(std::shared_ptr<Pers>);
      bool removePers(std::shared_ptr<Pers>);
      
    private:
      std::shared_ptr<Zglv> m_zglv;
      std::vector<std::shared_ptr<Pers> > m_pers;
    };

  }
}
