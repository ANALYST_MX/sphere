#pragma once

#include <string>

#include "type/Object.hxx"

namespace sphere
{
  namespace model
  {
    
    class Pacient
      : public sphere::type::Object
    {
    public:
      Pacient() = default;
      virtual ~Pacient();
      std::string getIdPac() const;
      int getVpolis() const;
      std::string getSpolis() const;
      std::string getNpolis() const;
      std::string getStOkato() const;
      std::string getSmo() const;
      std::string getSmoOgrn() const;
      std::string getSmoOk() const;
      std::string getSmoNam() const;
      std::string getPrinPol() const;
      std::string getNovor() const;
      int getVnovD() const;
      void setIdPac(const std::string &);
      void setVpolis(int);
      void setSpolis(const std::string &);
      void setNpolis(const std::string &);
      void setStOkato(const std::string &);
      void setSmo(const std::string &);
      void setSmoOgrn(const std::string &);
      void setSmoOk(const std::string &);
      void setSmoNam(const std::string &);
      void setPrinPol(const std::string &);
      void setNovor(const std::string &);
      void setVnovD(int);
    private:
      std::string m_idPac;
      int m_vpolis;
      std::string m_spolis;
      std::string m_npolis;
      std::string m_stOkato;
      std::string m_smo;
      std::string m_smoOgrn;
      std::string m_smoOk;
      std::string m_smoNam;
      std::string m_prinPol;
      std::string m_novor;
      int m_vnovD;
    };

  }
}
	    
