#pragma once

#include <vector>
#include <memory>

#include "type/Object.hxx"
#include "model/Zglv.hxx"
#include "model/Schet.hxx"
#include "model/Zap.hxx"

namespace sphere
{
  namespace model
  {
    
    class ZlList
      : public sphere::type::Object
    {
    public:
      ZlList() = default;
      virtual ~ZlList();
      std::shared_ptr<Zglv> getZglv() const;
      std::shared_ptr<Schet> getSchet() const;
      std::vector<std::shared_ptr<Zap> > &getZap();
      const std::vector<std::shared_ptr<Zap> > &getZap() const;
      void setZglv(std::shared_ptr<Zglv>);
      void setSchet(std::shared_ptr<Schet>);
      bool addZap(std::shared_ptr<Zap>);
      bool removeZap(std::shared_ptr<Zap>);
    private:
      std::shared_ptr<Zglv> m_zglv;
      std::shared_ptr<Schet> m_schet;
      std::vector<std::shared_ptr<Zap> > m_zap;
    };

  }
}
