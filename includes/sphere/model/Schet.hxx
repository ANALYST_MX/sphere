#pragma once

#include <string>

#include "type/Object.hxx"
#include "type/Money.hxx"
#include "type/Date.hxx"

namespace sphere
{
  namespace model
  {

    class Schet
      : public sphere::type::Object
    {
    public:
      Schet() = default;
      virtual ~Schet();
      int getCode() const;
      std::string getCodeMo() const;
      int getPodr() const;
      int getYear() const;
      int getMonth() const;
      std::string getNschet() const;
      sphere::type::Date getDschet() const;
      std::string getPlat() const;
      sphere::type::Money getSummav() const;
      sphere::type::Money getSummavR() const;
      sphere::type::Money getSummavF() const;
      std::string getComents() const;
      sphere::type::Money getSummap() const;
      sphere::type::Money getSummapR() const;
      sphere::type::Money getSummapF() const;
      sphere::type::Money getSankMek() const;
      sphere::type::Money getSankMee() const;
      sphere::type::Money getSankEkmp() const;
      std::string getDisp() const;
      void setCode(int);
      void setCodeMo(const std::string &);
      void setPodr(int);
      void setYear(int);
      void setMonth(int);
      void setNschet(const std::string &);
      void setDschet(const sphere::type::Date &);
      void setPlat(const std::string &);
      void setSummav(const sphere::type::Money &);
      void setSummavR(const sphere::type::Money &);
      void setSummavF(const sphere::type::Money &);
      void setComents(const std::string &);
      void setSummap(const sphere::type::Money &);
      void setSummapR(const sphere::type::Money &);
      void setSummapF(const sphere::type::Money &);
      void setSankMek(const sphere::type::Money &);
      void setSankMee(const sphere::type::Money &);
      void setSankEkmp(const sphere::type::Money &);
      void setDisp(const std::string &);
    private:
      int m_code;
      std::string m_codeMo;
      int m_podr;
      int m_year;
      int m_month;
      std::string m_nschet;
      sphere::type::Date m_dschet;
      std::string m_plat;
      sphere::type::Money m_summav;
      sphere::type::Money m_summavR;
      sphere::type::Money m_summavF;
      std::string m_coments;
      sphere::type::Money m_summap;
      sphere::type::Money m_summapR;
      sphere::type::Money m_summapF;
      sphere::type::Money m_sankMek;
      sphere::type::Money m_sankMee;
      sphere::type::Money m_sankEkmp;
      std::string m_disp;
    };
    
  }
}
	    
