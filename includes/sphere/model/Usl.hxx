#pragma once

#include <string>

#include "type/Object.hxx"
#include "type/Money.hxx"
#include "type/Date.hxx"

namespace sphere
{
  namespace model
  {
    
    class Usl
      : public sphere::type::Object
    {
    public:
      Usl() = default;
      virtual ~Usl();
      std::string getIdserv() const;
      std::string getLpu() const;
      int getLpu1() const;
      int getPodr() const;
      int getProfil() const;
      std::string getVidVme() const;
      int getDet() const;
      sphere::type::Date getDateIn() const;
      sphere::type::Date getDateOut() const;
      std::string getDs() const;
      std::string getCodeUsl() const;
      double getKolUsl() const;
      sphere::type::Money getTarif() const;
      sphere::type::Money getSumvUsl() const;
      int getPrvs() const;
      std::string getCodeMd() const;
      std::string getComentu() const;
      void setIdserv(const std::string &);
      void setLpu(const std::string &);
      void setLpu1(int);
      void setPodr(int);
      void setProfil(int);
      void setVidVme(const std::string &);
      void setDet(int);
      void setDateIn(const sphere::type::Date &);
      void setDateOut(const sphere::type::Date &);
      void setDs(const std::string &);
      void setCodeUsl(const std::string &);
      void setKolUsl(double);
      void setTarif(const sphere::type::Money &);
      void setSumvUsl(const sphere::type::Money &);
      void setPrvs(int);
      void setCodeMd(const std::string &);
      void setComentu(const std::string &);
    private:
      std::string m_idserv;
      std::string m_lpu;
      int m_lpu1;
      int m_podr;
      int m_profil;
      std::string m_vidVme;
      int m_det;
      sphere::type::Date m_dateIn;
      sphere::type::Date m_dateOut;
      std::string m_ds;
      std::string m_codeUsl;
      double m_kolUsl;
      sphere::type::Money m_tarif;
      sphere::type::Money m_sumvUsl;
      int m_prvs;
      std::string m_codeMd;
      std::string m_comentu;
    };
    
  }
}
	    
