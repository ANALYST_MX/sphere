#pragma once

#include <string>

#include "type/Object.hxx"
#include "type/Date.hxx"

namespace sphere
{
  namespace model
  {
  
    class Pers
      : public sphere::type::Object
    {
    public:
      Pers() = default;
      virtual ~Pers();
      std::string getIdPac() const;
      std::string getFam() const;
      std::string getIm() const;
      std::string getOt() const;
      int getW() const;
      sphere::type::Date getDr() const;
      int getDost() const;
      std::string getFamP() const;
      std::string getImP() const;
      std::string getOtP() const;
      int getWP() const;
      sphere::type::Date getDrP() const;
      int getDostP() const;
      std::string getMr() const;
      std::string getDoctype() const;
      std::string getDocser() const;
      std::string getDocnum() const;
      std::string getSnils() const;
      int getStatus() const;
      std::string getOksm() const;
      std::string getOkatog() const;
      std::string getOkatop() const;
      int getZip() const;
      std::string getArea() const;
      std::string getRegion() const;
      std::string getRegCity() const;
      std::string getItem() const;
      int getTypeItem() const;
      int getTypeUl() const;
      std::string getStreet() const;
      int getHouse() const;
      std::string getLiter() const;
      std::string getFlat() const;
      std::string getComentp() const;
      void setIdPac(const std::string &);
      void setFam(const std::string &);
      void setIm(const std::string &);
      void setOt(const std::string &);
      void setW(int);
      void setDr(const sphere::type::Date &);
      void setDost(int);
      void setFamP(const std::string &);
      void setImP(const std::string &);
      void setOtP(const std::string &);
      void setWP(int);
      void setDrP(const sphere::type::Date &);
      void setDostP(int);
      void setMr(const std::string &);
      void setDoctype(const std::string &);
      void setDocser(const std::string &);
      void setDocnum(const std::string &);
      void setSnils(const std::string &);
      void setStatus(int);
      void setOksm(const std::string &);
      void setOkatog(const std::string &);
      void setOkatop(const std::string &);
      void setZip(int);
      void setArea(const std::string &);
      void setRegion(const std::string &);
      void setRegCity(const std::string &);
      void setItem(const std::string &);
      void setTypeItem(int);
      void setTypeUl(int);
      void setStreet(const std::string &);
      void setHouse(int);
      void setLiter(const std::string &);
      void setFlat(const std::string &);
      void setComentp(const std::string &);
    private:
      std::string m_idPac;
      std::string m_fam;
      std::string m_im;
      std::string m_ot;
      int m_w;
      sphere::type::Date m_dr;
      int m_dost;
      std::string m_famP;
      std::string m_imP;
      std::string m_otP;
      int m_wP;
      sphere::type::Date m_drP;
      int m_dostP;
      std::string m_mr;
      std::string m_doctype;
      std::string m_docser;
      std::string m_docnum;
      std::string m_snils;
      int m_status;
      std::string m_oksm;
      std::string m_okatog;
      std::string m_okatop;
      int m_zip;
      std::string m_area;
      std::string m_region;
      std::string m_regCity;
      std::string m_item;
      int m_typeItem;
      int m_typeUl;
      std::string m_street;
      int m_house;
      std::string m_liter;
      std::string m_flat;
      std::string m_comentp;
    };

  }
}
