#pragma once

#include <string>
#include <memory>

#include "type/Object.hxx"
#include "type/Money.hxx"
#include "type/Date.hxx"
#include "model/Usl.hxx"
#include "model/Sank.hxx"

namespace sphere
{
  namespace model
  {

    class Sluch
      : public sphere::type::Object
    {
    public:
      Sluch() = default;
      virtual ~Sluch();
      std::string getIdcase() const;
      int getUslOk() const;
      int getVidpom() const;
      int getForPom() const;
      std::string getVidHmp() const;
      int getMetodHmp() const;
      std::string getNprMo() const;
      int getExtr() const;
      std::string getLpu() const;
      std::string getLpu1() const;
      int getLpuDep() const;
      int getAmbDep() const;
      int getProfil() const;
      int getDet() const;
      std::string getNhistory() const;
      sphere::type::Date getDate1() const;
      sphere::type::Date getDate2() const;
      std::string getDs0() const;
      std::string getDs1() const;
      std::string getDs2() const;
      std::string getDs3() const;
      int getVnovM() const;
      std::string getCodeMes1() const;
      std::string getCodeMes2() const;
      int getRslt() const;
      int getIshod() const;
      int getPrvs() const;
      std::string getIddokt() const;
      int getIdsp() const;
      double getEdCol() const;
      int getReanD() const;
      double getKskpCoef() const;
      int getKpg() const;
      int getKsg() const;
      sphere::type::Money getTarifR() const;
      sphere::type::Money getTarifF() const;
      sphere::type::Money getSumv() const;
      sphere::type::Money getSumvR() const;
      sphere::type::Money getSumvF() const;
      int getOplata() const;
      sphere::type::Money getSump() const;
      sphere::type::Money getSumpR() const;
      sphere::type::Money getSumpF() const;
      sphere::type::Money getSankIt() const;
      std::vector<std::shared_ptr<Sank> > &getSank();
      const std::vector<std::shared_ptr<Sank> > &getSank() const;
      std::string getComents() const;
      int getOsSluch() const;
      std::vector<std::shared_ptr<Usl> > &getUsl();
      const std::vector<std::shared_ptr<Usl> > &getUsl() const;
      int getRefreason() const;
      void setIdcase(const std::string &);
      void setUslOk(int);
      void setVidpom(int);
      void setForPom(int);
      void setVidHmp(const std::string &);
      void setMetodHmp(int);
      void setNprMo(const std::string &);
      void setExtr(int);
      void setLpu(const std::string &);
      void setLpu1(const std::string &);
      void setLpuDep(int);
      void setAmbDep(int);
      void setProfil(int);
      void setDet(int);
      void setNhistory(const std::string &);
      void setDate1(const sphere::type::Date &);
      void setDate2(const sphere::type::Date &);
      void setDs0(const std::string &);
      void setDs1(const std::string &);
      void setDs2(const std::string &);
      void setDs3(const std::string &);
      void setVnovM(int);
      void setCodeMes1(const std::string &);
      void setCodeMes2(const std::string &);
      void setRslt(int);
      void setIshod(int);
      void setPrvs(int);
      void setIddokt(const std::string &);
      void setIdsp(int);
      void setEdCol(double);
      void setReanD(int);
      void setKskpCoef(double);
      void setKpg(int);
      void setKsg(int);
      void setTarifR(const sphere::type::Money &);
      void setTarifF(const sphere::type::Money &);
      void setSumv(const sphere::type::Money &);
      void setSumvR(const sphere::type::Money &);
      void setSumvF(const sphere::type::Money &);
      void setOplata(int);
      void setSump(const sphere::type::Money &);
      void setSumpR(const sphere::type::Money &);
      void setSumpF(const sphere::type::Money &);
      void setSankIt(const sphere::type::Money &);
      bool addSank(std::shared_ptr<Sank>);
      bool removeSank(std::shared_ptr<Sank>);
      void setComents(const std::string &);
      void setOsSluch(int);
      bool addUsl(std::shared_ptr<Usl>);
      bool removeUsl(std::shared_ptr<Usl>);
      void setRefreason(int);
    private:
      std::string m_idcase;
      int m_uslOk;
      int m_vidpom;
      int m_forPom;
      std::string m_vidHmp;
      int m_metodHmp;
      std::string m_nprMo;
      int m_extr;
      std::string m_lpu;
      std::string m_lpu1;
      int m_lpuDep;
      int m_ambDep;
      int m_profil;
      int m_det;
      std::string m_nhistory;
      sphere::type::Date m_date1;
      sphere::type::Date m_date2;
      std::string m_ds0;
      std::string m_ds1;
      std::string m_ds2;
      std::string m_ds3;
      int m_vnovM;
      std::string m_codeMes1;
      std::string m_codeMes2;
      int m_rslt;
      int m_ishod;
      int m_prvs;
      std::string m_iddokt;
      int m_idsp;
      double m_edCol;
      int m_reanD;
      double m_kskpCoef;
      int m_kpg;
      int m_ksg;
      sphere::type::Money m_tarifR;
      sphere::type::Money m_tarifF;
      sphere::type::Money m_sumv;
      sphere::type::Money m_sumvR;
      sphere::type::Money m_sumvF;
      int m_oplata;
      sphere::type::Money m_sump;
      sphere::type::Money m_sumpR;
      sphere::type::Money m_sumpF;
      sphere::type::Money m_sankIt;
      std::vector<std::shared_ptr<Sank> > m_sank;
      std::string m_coments;
      int m_osSluch;
      std::vector<std::shared_ptr<Usl> > m_usl;
      int m_refreason;
    };
      
  }
}

