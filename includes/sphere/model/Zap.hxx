#pragma once

#include <string>
#include <memory>

#include "type/Object.hxx"
#include "model/Pacient.hxx"
#include "model/Sluch.hxx"

namespace sphere
{
  namespace model
  {
    
    class Zap
      : public sphere::type::Object
    {
    public:
      Zap() = default;
      virtual ~Zap();
      int getNZap() const;
      int getPrNov() const;
      std::shared_ptr<Pacient> getPacient() const;
      std::shared_ptr<Sluch> getSluch() const;
      void setNZap(int);
      void setPrNov(int);
      void setPacient(std::shared_ptr<Pacient>);
      void setSluch(std::shared_ptr<Sluch>);
    private:
      int m_nZap;
      int m_prNov;
      std::shared_ptr<Pacient> m_pacient;
      std::shared_ptr<Sluch> m_sluch;
    };

  }
}
	    
