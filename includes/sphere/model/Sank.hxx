#pragma once

#include <string>

#include "type/Object.hxx"
#include "type/Money.hxx"

namespace sphere
{
  namespace model
  {
    
    class Sank
      : public sphere::type::Object
    {
    public:
      Sank() = default;
      virtual ~Sank();
      std::string getSCode() const;
      sphere::type::Money getSSum() const;
      int getSTip() const;
      int getSOsn() const;
      std::string getSCom() const;
      int getSIst() const;
      void setSCode(const std::string &);
      void setSSum(const sphere::type::Money &);
      void setSTip(int);
      void setSOsn(int);
      void setSCom(const std::string &);
      void setSIst(int);
    private:
      std::string m_sCode;
      sphere::type::Money m_sSum;
      int m_sTip;
      int m_sOsn;
      std::string m_sCom;
      int m_sIst;
    };
      
  }
}
	    
